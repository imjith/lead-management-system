package in.sjstudio.lms;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import in.sjstudio.lms.controller.AppController;
import in.sjstudio.lms.controller.RestApiController;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTests {

	@Autowired
	AppController appController;

	@Autowired
	RestApiController restApiController;

	@Autowired
	MockMvc mockMvc;

	@Test
	public void contextLoads() {
		assertThat(appController).isNotNull();
		assertThat(restApiController).isNotNull();
	}

	@Test
	public void shouldShowDefaultPage() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

}
