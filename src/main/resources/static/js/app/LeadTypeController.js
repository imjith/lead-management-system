'use strict'

var app = angular.module("app", []);

app.controller('leadTypeController', function($scope, $http) {

	$scope.fetchLeadTypes = function() {
		$http({
			method : 'GET',
			url : 'api/lead-types'
		}).then(function success(response) {
			$scope.leadTypes = response.data;
			console.log(response.data);
		}, function error(response) {
			console.log("Failed to retrieve lead types")
		});
	}

	$scope.addLeadType = function(leadType) {

		$('#btnAddLeadtype').attr('disabled', true).text('Adding');

		$http({
			method : 'POST',
			url : 'api/lead-type',
			data : leadType
		}).then(function success(response) {
			$scope.fetchLeadTypes();
			$('#btnAddLeadtype').attr('disabled', false).text('Add');
		}, function error(response) {
			console.log("Failed to retrieve lead types");
			$('#btnAddLeadtype').attr('disabled', false).text('Add');
		});

	};
});