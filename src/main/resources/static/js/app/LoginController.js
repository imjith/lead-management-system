'use strict'

var app = angular.module("app", []);

app.controller('loginController', function($scope, $http) {

	var setSucess = function($isSuccess) {
		$scope.isFailure = !$isSuccess;
	}

	$scope.login = function($user) {
		$http({
			method : 'POST',
			url : 'api/login',
			params : $user
		}).then(function success(response) {
			var data = response.data;
			console.log(data);
			if (data.status == 1) {
				window.location = "dashboard";
			} else {
				setSucess(false);
				$scope.errorMessage = data.message;
			}

		}, function error(response) {
			setSucess(false);
			$scope.errorMessage = data.message;
		});
	}
});