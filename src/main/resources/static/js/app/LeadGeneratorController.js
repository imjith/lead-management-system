'use strict'

var app = angular.module("app", []);

app.controller('leadGeneratorController', function($scope, $http) {
	$http({
		method : 'POST',
		url : 'api/lead-generators'
	}).then(function success(response) {
		console.log("sucess");
		$scope.leadGenerators = response.data;
	}, function error(response) {
		console.log("Failed to retrieve lead generators")
	});
});