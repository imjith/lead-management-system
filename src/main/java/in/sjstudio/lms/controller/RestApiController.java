package in.sjstudio.lms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import in.sjstudio.lms.model.LeadGenerator;
import in.sjstudio.lms.model.LeadType;
import in.sjstudio.lms.model.User;
import in.sjstudio.lms.service.LeadGeneratorService;
import in.sjstudio.lms.service.LeadTypeService;
import in.sjstudio.lms.service.UserService;

@RestController
@RequestMapping("api/")
public class RestApiController {

	@Autowired
	private UserService userService;

	@Autowired
	private LeadGeneratorService leadGeneratorService;

	@Autowired
	private LeadTypeService leadTypeService;

	@GetMapping("test")
	public String greetings() {
		return "greeting";
	}

	@PostMapping("login")
	public String login(@RequestParam("username") String uname, @RequestParam("password") String pwd) {
		User user = userService.findUserByName(uname);
		if (null != user) {
			if (user.getPassword().equals(pwd)) {
				return String.format("{\"status\":\"1\",\"uname\":\"%s\",\"role\":\"%s\"}", user.getUsername(),
						user.getRole());
			} else {
				return "{\"status\":\"0\",\"message\":\"Password doensn't match!\"}";
			}
		}
		return "{\"status\":\"0\",\"message\":\"User not found!\"}";
	}

	@GetMapping("lead-types")
	public List<LeadType> leadTypes() {
		return leadTypeService.findAllLeadTypes();
	}

	@GetMapping("lead-type/{id}")
	public LeadType leadTypeById(@PathVariable("id") Long id) {
		return leadTypeService.findLeadTypeById(id);
	}

	@GetMapping("lead-type/{type}")
	public LeadType leadTypeById(@PathVariable("type") String type) {
		return leadTypeService.findLeadTypeByType(type);
	}

	@PostMapping("lead-type")
	public LeadType addLeadType(@RequestBody LeadType leadType) {
		System.out.println("Saving lead type");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		leadTypeService.saveLeadType(leadType);
		return leadType;
	}

	@GetMapping("lead-generators")
	public List<LeadGenerator> leadGenerators() {
		return leadGeneratorService.findAllLeadGenerators();
	}

	@GetMapping("lead-generators/{id}")
	public LeadGenerator leadGeneratorById(@PathVariable("id") Long id) {
		return leadGeneratorService.findLeadGeneratorById(id);
	}

	@GetMapping("lead-generators/{name}")
	public LeadGenerator leadGeneratorByName(@PathVariable("name") String name) {
		return leadGeneratorService.findLeadGeneratorByName(name);
	}

}
