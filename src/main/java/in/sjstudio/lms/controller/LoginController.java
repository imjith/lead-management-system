package in.sjstudio.lms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import in.sjstudio.lms.model.User;
import in.sjstudio.lms.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@PostMapping("login")
	public String login(@ModelAttribute("user") User user, Model model) {
		User u = userService.findUserByName(user.getUsername());
		if (null != u) {
			if (u.getPassword().equals(user.getPassword())) {
				return "redirect:dashboard";
			} else {
				model.addAttribute("isError", true);
				model.addAttribute("errorMessage", "Password is incorrect.");
				return "login";
			}
		}
		model.addAttribute("isError", true);
		model.addAttribute("errorMessage", "User not found!");
		return "login";
	}

}
