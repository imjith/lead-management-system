package in.sjstudio.lms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import in.sjstudio.lms.model.LeadType;
import in.sjstudio.lms.service.LeadTypeService;

@Controller
public class LeadController {

	@Autowired
	LeadTypeService leadTypeService;

	@PostMapping("add-lead-type")
	public String addLeadType(@ModelAttribute("lt") LeadType leadType, Model model) {

		leadTypeService.saveLeadType(leadType);

		return "redirect:lead-type";

	}

}
