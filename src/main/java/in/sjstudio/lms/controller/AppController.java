package in.sjstudio.lms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import in.sjstudio.lms.model.LeadType;
import in.sjstudio.lms.model.User;

@Controller
public class AppController {

	@GetMapping("/")
	public String defaultPage(@ModelAttribute("user") User user, Model model) {
		return "login";
	}

	@GetMapping(value = { "index", "dashboard.html", "index.html" })
	public String handleDashboardUrls(Model model) {
		return "redirect:/dashboard";
	}

	@GetMapping("dashboard")
	public String showDashboard(Model model) {
		return "index";
	}

	@GetMapping(value = { "lead-generator.html" })
	public String handleLeadGeneratorUrls(Model model) {
		return "redirect:/lead-generator";
	}

	@GetMapping("lead-generator")
	public String showLeadGeneratorPage(Model model) {
		return "lead-generator";
	}

	@GetMapping(value = { "lead-type.html" })
	public String handleLeadTypeUrls(Model model) {
		return "redirect:/lead-type";
	}

	@GetMapping("lead-type")
	public String showLeadTypePage(@ModelAttribute("lt") LeadType leadType, Model model) {
		return "lead-type";
	}

	@GetMapping(value = { "lead-data.html" })
	public String handleLeadDataUrls(Model model) {
		return "redirect:/lead-data";
	}

	@GetMapping("lead-data")
	public String showLeadDataPage(Model model) {
		return "lead-data";
	}
}
