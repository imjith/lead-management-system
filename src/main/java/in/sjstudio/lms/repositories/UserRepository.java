package in.sjstudio.lms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.sjstudio.lms.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findUserByUsername(String userName);
}
