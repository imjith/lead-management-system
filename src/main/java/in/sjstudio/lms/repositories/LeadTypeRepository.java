package in.sjstudio.lms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.sjstudio.lms.model.LeadType;

public interface LeadTypeRepository extends JpaRepository<LeadType, Long> {
	LeadType findByLeadType(String leadType);
}
