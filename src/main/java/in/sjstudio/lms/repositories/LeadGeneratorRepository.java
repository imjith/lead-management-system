package in.sjstudio.lms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.sjstudio.lms.model.LeadGenerator;

public interface LeadGeneratorRepository extends JpaRepository<LeadGenerator, Long> {
	LeadGenerator findByName(String name);
}
