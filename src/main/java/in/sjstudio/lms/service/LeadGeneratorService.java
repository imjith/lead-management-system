package in.sjstudio.lms.service;

import java.util.List;

import in.sjstudio.lms.model.LeadGenerator;

public interface LeadGeneratorService {

	LeadGenerator findLeadGeneratorById(Long id);

	LeadGenerator findLeadGeneratorByName(String name);

	List<LeadGenerator> findAllLeadGenerators();

	void saveLeadGenerator(LeadGenerator leadGenerator);

	void updateLeadGenerator(LeadGenerator leadGenerator);

	void deleteLeadGenerator(LeadGenerator leadGenerator);

}
