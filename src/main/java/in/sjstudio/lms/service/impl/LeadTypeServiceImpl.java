package in.sjstudio.lms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sjstudio.lms.model.LeadType;
import in.sjstudio.lms.repositories.LeadTypeRepository;
import in.sjstudio.lms.service.LeadTypeService;

@Service
public class LeadTypeServiceImpl implements LeadTypeService {

	@Autowired
	private LeadTypeRepository leadTypeRepo;

	@Override
	public LeadType findLeadTypeById(Long id) {
		return leadTypeRepo.findById(id).get();
	}

	@Override
	public LeadType findLeadTypeByType(String type) {
		return leadTypeRepo.findByLeadType(type);
	}

	@Override
	public List<LeadType> findAllLeadTypes() {
		return leadTypeRepo.findAll();
	}

	@Override
	public void saveLeadType(LeadType leadType) {
		leadTypeRepo.save(leadType);
	}

	@Override
	public void updateLeadType(LeadType leadType) {
		saveLeadType(leadType);
	}

	@Override
	public void deleteLeadType(LeadType leadType) {
		leadTypeRepo.delete(leadType);
	}

}
