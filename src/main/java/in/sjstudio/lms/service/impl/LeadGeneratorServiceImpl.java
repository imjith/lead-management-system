package in.sjstudio.lms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.sjstudio.lms.model.LeadGenerator;
import in.sjstudio.lms.repositories.LeadGeneratorRepository;
import in.sjstudio.lms.service.LeadGeneratorService;

@Service
public class LeadGeneratorServiceImpl implements LeadGeneratorService {

	@Autowired
	private LeadGeneratorRepository leadGeneratorRepo;

	@Override
	public LeadGenerator findLeadGeneratorById(Long id) {
		return leadGeneratorRepo.findById(id).get();
	}

	@Override
	public LeadGenerator findLeadGeneratorByName(String name) {
		return leadGeneratorRepo.findByName(name);
	}

	@Override
	public List<LeadGenerator> findAllLeadGenerators() {
		return leadGeneratorRepo.findAll();
	}

	@Override
	public void saveLeadGenerator(LeadGenerator leadGenerator) {
		leadGeneratorRepo.save(leadGenerator);

	}

	@Override
	public void updateLeadGenerator(LeadGenerator leadGenerator) {
		saveLeadGenerator(leadGenerator);
	}

	@Override
	public void deleteLeadGenerator(LeadGenerator leadGenerator) {
		leadGeneratorRepo.delete(leadGenerator);
	}

}
