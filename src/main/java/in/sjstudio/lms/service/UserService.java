package in.sjstudio.lms.service;

import java.util.List;

import in.sjstudio.lms.model.User;

public interface UserService {

	User findUserById(Long id);

	User findUserByName(String userName);

	List<User> findAllUsers();

	void saveUser(User user);

	void updateUser(User user);

	void delete(User user);

}
