package in.sjstudio.lms.service;

import java.util.List;

import in.sjstudio.lms.model.LeadType;

public interface LeadTypeService {

	LeadType findLeadTypeById(Long id);

	LeadType findLeadTypeByType(String type);

	List<LeadType> findAllLeadTypes();

	void saveLeadType(LeadType leadType);

	void updateLeadType(LeadType leadType);

	void deleteLeadType(LeadType leadType);

}
